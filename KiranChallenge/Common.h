//
//  Common.h
//  KiranChallenge
//
//  Created by DAXX on 4/19/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#ifndef Common_h
#define Common_h

// uncomment it for debug purposes
// #define ENABLE_LOGGING

#ifdef ENABLE_LOGGING
#   define NSLog(...) NSLog(__VA_ARGS__)
#else
#   define NSLog(...)
#endif

#endif /* Common_h */
