//
//  PatchworkLayout.h
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "CachedAbstractLayot.h"


@protocol PatchworkLayoutDelegate<UICollectionViewDelegateFlowLayout>

- (NSUInteger)numberOfColumsInRow:(NSUInteger)row;
- (NSUInteger)numberOfRowsInColumn:(NSUInteger)column forRow:(NSUInteger)row;
- (CGFloat)rowHeight;

@end


@interface PatchworkLayout : CachedAbstractLayot

@property (nonatomic, weak) id<PatchworkLayoutDelegate> delegate;

@end
