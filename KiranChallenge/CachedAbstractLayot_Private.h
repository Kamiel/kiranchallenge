//
//  CachedAbstractLayot_Private.h
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "CachedAbstractLayot.h"


@interface CachedAbstractLayot () {
    NSMutableDictionary<NSIndexPath *, NSValue *> *_cellSizes;
    NSMutableDictionary<NSIndexPath *, NSValue *> *_cellOffsets;
    NSMutableDictionary<NSValue *, NSIndexPath *> *_indexPathsByFrame;
}

@end
