//
//  StackLayout.m
//  KiranChallenge
//
//  Created by DAXX on 4/8/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "StackLayout.h"
#import "CachedAbstractLayot+Cach.h"
#include "Common.h"


@interface StackLayout ()

// helper methods
- (CGFloat)verticalOffsetForIndexPath:(NSIndexPath *)indexPath;
- (CGPoint)offsetForIndexPath:(NSIndexPath *)indexPath forWidth:(CGFloat)width;
- (CGSize)sizeForIndexPath:(NSIndexPath *)indexPath;

@end


@implementation StackLayout

#pragma mark - override CachedAbstractLayot methods

- (CGSize)collectionViewContentSize
{
    CGSize contentSize = [super collectionViewContentSize];
    
    // update according to cells count and sizes
    if ([self.collectionView.dataSource
         respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
        NSUInteger rows = [self.collectionView.dataSource collectionView:self.collectionView
                                                  numberOfItemsInSection:0];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rows - 1
                                                    inSection:0];
        CGFloat lastCellOffset = [self verticalOffsetForIndexPath:indexPath];
        CGFloat lastCellHeight = [self sizeForIndexPath:indexPath].height;
        contentSize.height = MAX(contentSize.height,
                                 (lastCellOffset + lastCellHeight + self.lineSpacing));
    }
    return contentSize;
}

- (CGRect)frameForIndexPath:(NSIndexPath *)indexPath
{
    CGSize  size   = [self sizeForIndexPath:indexPath];
    CGPoint offset = [self offsetForIndexPath:indexPath forWidth:size.width];
    CGRect  frame  = {offset, size};
    
    NSValue *keyFrame = [NSValue valueWithCGRect:frame];
    self.indexPathsByFrame[keyFrame] = indexPath;
    
    return frame;
}

#pragma mark - private methods

- (CGFloat)verticalOffsetForIndexPath:(NSIndexPath *)indexPath
{
    CGFloat verticalOffset = self.lineSpacing;
    
    for (NSUInteger row = 0; row < indexPath.row; row++) {
        CGSize cellSize = [self sizeForIndexPath:[NSIndexPath indexPathForRow:row
                                                                    inSection:indexPath.section]];
        verticalOffset += cellSize.height + self.lineSpacing;
    }
    return verticalOffset;
}

- (CGPoint)offsetForIndexPath:(NSIndexPath *)indexPath forWidth:(CGFloat)width
{
    NSValue *offsetValue = self.cellOffsets[indexPath];
    
    if (offsetValue) {
        return [offsetValue CGPointValue];
    }
    CGFloat horizontalOffset = (self.collectionViewContentSize.width - width) / 2;
    CGPoint offset = CGPointMake(horizontalOffset,
                                 [self verticalOffsetForIndexPath:indexPath]);
    self.cellOffsets[indexPath] = [NSValue valueWithCGPoint:offset];
    
    return offset;
}

- (CGSize)sizeForIndexPath:(NSIndexPath *)indexPath
{
    NSValue *sizeValue = self.cellSizes[indexPath];
    
    if (sizeValue) {
        return [sizeValue CGSizeValue];
    }
    CGSize size = ([self.collectionView.delegate
                    respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)])
    ? [(id<UICollectionViewDelegateFlowLayout>)self.collectionView.delegate collectionView:self.collectionView
                                                                                    layout:self
                                                                    sizeForItemAtIndexPath:indexPath]
    : CGSizeZero; // ???: make default size here, or Assert
    
    self.cellSizes[indexPath] = [NSValue valueWithCGSize:size];
    
    return size;
}

@end
