//
//  ImageCell.m
//  KiranChallenge
//
//  Created by DAXX on 4/7/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "ImageCell.h"

@import AFNetworking;

#include "Common.h"


static const NSTimeInterval kFadeDuration = 0.3;


@implementation ImageCell

+ (UIImage *)defaultImage { return [UIImage imageNamed:@"Cloud"]; }
+ (UIImage *)failureImage { return [UIImage imageNamed:@"OhNo"]; }

- (instancetype)cellWithImageFromURL:(NSURL *)URL
                          completion:(void (^)())completion
{
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    
    [self.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:URL]
                          placeholderImage:[ImageCell defaultImage]
                                   success:^(NSURLRequest *request,
                                             NSHTTPURLResponse * _Nullable response,
                                             UIImage *image) {
                                       [UIView transitionWithView:self.imageView
                                                         duration:kFadeDuration
                                                          options:UIViewAnimationOptionTransitionCrossDissolve
                                                       animations:^{
                                                           self.imageView.image = image;
                                                       } completion:nil];
                                       [self.activityIndicator stopAnimating];
                                       self.activityIndicator.hidden = YES;
                                       // FIXME: animate previous image size first, than make fade animation
                                       if (completion) {
                                           completion();
                                       }
                                   }
                                   failure:^(NSURLRequest *request,
                                             NSHTTPURLResponse * _Nullable response,
                                             NSError *error) {
                                       [UIView transitionWithView:self.imageView
                                                         duration:kFadeDuration
                                                          options:UIViewAnimationOptionTransitionCrossDissolve
                                                       animations:^{
                                                           self.imageView.image = [ImageCell failureImage];
                                                       } completion:nil];
                                       [self.activityIndicator stopAnimating];
                                       self.activityIndicator.hidden = YES;
                                       
                                       if (completion) {
                                           completion();
                                       }
                                       NSLog(@"Can't get an image by URL %@ with error %@.",
                                             request.URL, error.description);
                                   }];
    return self;
}

// hack from here: https://rbnsn.me/uicollectionviewcell-auto-layout-performance
// speed up autolayout
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}

@end
