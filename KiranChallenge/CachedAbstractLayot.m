//
//  CachedAbstractLayot.m
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "CachedAbstractLayot.h"
#import "CachedAbstractLayot_Private.h"
#import "CachedAbstractLayot+Cach.h"
#include "Common.h"


static const CGFloat kDefaultLineSpacing      = 8.f;
static const CGFloat kDefaultInteritemSpacing = 8.f;

@interface CachedAbstractLayot (private)

// cache attributes and rect for each scrolling
@property (copy, nonatomic, readonly) NSMutableArray<UICollectionViewLayoutAttributes *> *previousLayoutAttributes;
@property (assign, nonatomic) CGRect previousLayoutRect;

@end

@implementation CachedAbstractLayot {
    NSMutableArray<UICollectionViewLayoutAttributes *> *_previousLayoutAttributes;
    CGRect _previousLayoutRect;
}

// abstract class
- (id)init
{
    self = [super init];
    
    if ([self isMemberOfClass:[CachedAbstractLayot class]]) {
        NSAssert(NO, @"Instantiate subclass only!");
        [self doesNotRecognizeSelector:_cmd];
        
        return nil;
    }
    if (self) {
        self.lineSpacing      = kDefaultLineSpacing;
        self.interitemSpacing = kDefaultInteritemSpacing;
    }
    return self;
}

#pragma mark - override UICollectionViewLayout methods

- (void)invalidateLayout
{
    // cleanup caches
    [self.cellSizes removeAllObjects];
    [self.cellOffsets removeAllObjects];
    [self.previousLayoutAttributes removeAllObjects];
    self.previousLayoutRect = CGRectZero;
    [self.indexPathsByFrame removeAllObjects];
    [super invalidateLayout];
}

- (CGSize)collectionViewContentSize
{
    CGSize contentSize = [super collectionViewContentSize];
    
    // update according to bounds
    contentSize = CGSizeMake(MAX(contentSize.width, self.collectionView.bounds.size.width),
                             MAX(contentSize.height, self.collectionView.bounds.size.height));
    return contentSize;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ({
        CGRect frame = [self frameForIndexPath:indexPath];
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes
                                                        layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = frame;
        NSLog(@"Attributes: %@", attributes);
        
        attributes;
    });
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)bounds
{
    if (CGRectEqualToRect(bounds, self.previousLayoutRect)) {
        return self.previousLayoutAttributes; // cached here
    }
    [self.previousLayoutAttributes removeAllObjects];
    self.previousLayoutRect = bounds;
    
#warning Weak point of the algorithm. Works slowly if there are more than 10000 cells
    
    [self.cellSizes enumerateKeysAndObjectsUsingBlock:^(NSIndexPath * _Nonnull key, NSValue * _Nonnull sizeValue, BOOL * _Nonnull stop) {
        CGSize  size   = [sizeValue CGSizeValue];
        NSValue *offsetValue = self.cellOffsets[key];
        
        if (!offsetValue) { // not cached yet
            return; // it not fully calculated for rect
        }
        CGPoint offset = [offsetValue CGPointValue];
        CGRect  frame  = {offset, size};
        
        if (CGRectIntersectsRect(frame, bounds)) {
            NSValue *keyFrame = [NSValue valueWithCGRect:frame];
            
            [self.previousLayoutAttributes addObject:
             [self layoutAttributesForItemAtIndexPath:self.indexPathsByFrame[keyFrame]]];
            NSLog(@"index path: row %li section %li", self.indexPathsByFrame[keyFrame].row, self.indexPathsByFrame[keyFrame].section);
        }
    }];
    return self.previousLayoutAttributes;
}

- (void)prepareLayout
{
    [super prepareLayout];
    
    if ([self.collectionView.dataSource
         respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
        NSUInteger rows = [self.collectionView.dataSource collectionView:self.collectionView
                                                  numberOfItemsInSection:0];
        // precalculate sizes
        for (NSUInteger item = 0; item < rows; item++) {
            NSIndexPath *path = [NSIndexPath indexPathForItem:item inSection:0];
            [self frameForIndexPath:path];
        }
    }
    NSLog(@"%@", self.indexPathsByFrame.description);
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return !(CGSizeEqualToSize(newBounds.size, self.collectionView.frame.size));
}

#pragma mark - properties

- (NSMutableArray<UICollectionViewLayoutAttributes *> *)previousLayoutAttributes
{
    if (!self->_previousLayoutAttributes) {
        // reserve space for array with onscreen items
        NSUInteger capacity = 7; // !!!: magic number
        self->_previousLayoutAttributes = [[NSMutableArray alloc] initWithCapacity:capacity];
    }
    return self->_previousLayoutAttributes;
}

- (CGRect)previousLayoutRect
{
    return self->_previousLayoutRect;
}

- (void)setPreviousLayoutRect:(CGRect)previousLayoutRect
{
    if (!CGRectEqualToRect(self->_previousLayoutRect, previousLayoutRect)) {
        self->_previousLayoutRect = previousLayoutRect;
    }
}

#pragma mark - public methods

- (CGRect)frameForIndexPath:(NSIndexPath *)indexPath
{
    NSAssert(NO, @"Subclasses need to overwrite this method");
    [self doesNotRecognizeSelector:_cmd];
    
    return CGRectNull;
}

@end
