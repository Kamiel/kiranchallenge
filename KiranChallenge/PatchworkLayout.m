//
//  PatchworkLayout.m
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "PatchworkLayout.h"
#import "CachedAbstractLayot+Cach.h"
#include "Common.h"


static const NSUInteger kVisualPathLength = 3ul; // row, column, row in column


@interface PatchworkLayout ()

// helper methods
- (CGFloat)verticalOffsetForVisualPath:(NSIndexPath *)visualPath;
- (CGPoint)offsetForVisualPath:(NSIndexPath *)visualPath;
- (CGSize)sizeForVisualPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)visualPathFromIndexPath:(NSIndexPath *)indexPath;

@end


@implementation PatchworkLayout

#pragma mark - override CachedAbstractLayot methods

- (CGSize)collectionViewContentSize
{
    CGSize contentSize = [super collectionViewContentSize];
    
    // update according to cells count and sizes
    if ([self.collectionView.dataSource
         respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
        NSUInteger rows = [self.collectionView.dataSource collectionView:self.collectionView
                                                  numberOfItemsInSection:0];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rows - 1
                                                    inSection:0];
        NSIndexPath *visualPath = [self visualPathFromIndexPath:indexPath];
        
        CGFloat lastCellOffset = [self verticalOffsetForVisualPath:visualPath];
        CGFloat lastCellHeight = [self sizeForVisualPath:visualPath].height;
        contentSize.height = MAX(contentSize.height,
                                 (lastCellOffset + lastCellHeight));
    }
    return contentSize;
}

- (CGRect)frameForIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *visualPath = [self visualPathFromIndexPath:indexPath];

    CGSize  size   = [self sizeForVisualPath:visualPath];
    CGPoint offset = [self offsetForVisualPath:visualPath];
    CGRect  frame  = {offset, size};

    NSValue *keyFrame = [NSValue valueWithCGRect:frame];
    self.indexPathsByFrame[keyFrame] = indexPath;

    return frame;
}

#pragma mark - private methods

- (CGFloat)verticalOffsetForVisualPath:(NSIndexPath *)visualPath
{
    if (![self.delegate respondsToSelector:@selector(rowHeight)]
        ||
        ![self.delegate respondsToSelector:@selector(numberOfRowsInColumn:forRow:)]) {
        NSAssert(NO, @"PatchworkLayoutDelegate should be set.");
        
        return 0.f;
    }
    CGFloat    rowHeight          = [self.delegate rowHeight];
    NSUInteger currentRow         = [visualPath indexAtPosition:0];
    NSUInteger currentColumn      = [visualPath indexAtPosition:1];
    NSUInteger currentRowInColumn = [visualPath indexAtPosition:2];
    NSUInteger rowsInColumn       = [self.delegate numberOfRowsInColumn:currentColumn
                                                                 forRow:currentRow];
    CGFloat    rowInColumnHeight  = MAX(0.f, rowHeight - self.lineSpacing * MAX(0, (NSInteger)rowsInColumn - 1))
                                  / rowsInColumn;

    CGFloat    rowInColumnOffset  = rowInColumnHeight * currentRowInColumn
                                  + self.lineSpacing * currentRowInColumn;
    CGFloat    rowOffset          = rowHeight * currentRow
                                  + self.lineSpacing * currentRow;
    
    CGFloat verticalOffset = rowInColumnOffset + rowOffset;

    return verticalOffset;
}

- (CGPoint)offsetForVisualPath:(NSIndexPath *)visualPath
{
    NSValue *offsetValue = self.cellOffsets[visualPath];
    
    if (offsetValue) {
        return [offsetValue CGPointValue];
    }
    if (![self.delegate respondsToSelector:@selector(numberOfColumsInRow:)]) {
        NSAssert(NO, @"PatchworkLayoutDelegate should be set.");
        
        return CGPointZero;
    }
    NSUInteger currentRow    = [visualPath indexAtPosition:0];
    NSUInteger currentColumn = [visualPath indexAtPosition:1];
    NSUInteger columns       = [self.delegate numberOfColumsInRow:currentRow];
    CGFloat    columnWidth   = MAX(0.f, self.collectionView.bounds.size.width
                                   - self.interitemSpacing * MAX(0, (NSInteger)columns - 1)) / columns;
    CGFloat horizontalOffset = columnWidth * currentColumn
                             + self.interitemSpacing * currentColumn;
    CGPoint offset           = CGPointMake(horizontalOffset,
                                           [self verticalOffsetForVisualPath:visualPath]);
    self.cellOffsets[visualPath] = [NSValue valueWithCGPoint:offset];

    return offset;
}

- (CGSize)sizeForVisualPath:(NSIndexPath *)visualPath
{
    NSValue *sizeValue = self.cellSizes[visualPath];
    
    if (sizeValue) {
        return [sizeValue CGSizeValue];
    }
    if (![self.delegate respondsToSelector:@selector(rowHeight)]
        ||
        ![self.delegate respondsToSelector:@selector(numberOfColumsInRow:)]
        ||
        ![self.delegate respondsToSelector:@selector(numberOfRowsInColumn:forRow:)]) {
        NSAssert(NO, @"PatchworkLayoutDelegate should be set.");
        
        return CGSizeZero;
    }
    CGFloat    rowHeight     = [self.delegate rowHeight];
    NSUInteger currentRow    = [visualPath indexAtPosition:0];
    NSUInteger currentColumn = [visualPath indexAtPosition:1];
    NSUInteger rowsInColumn  = [self.delegate numberOfRowsInColumn:currentColumn
                                                            forRow:currentRow];
    NSUInteger columns       = [self.delegate numberOfColumsInRow:currentRow];

    // FIXME: higher cells should be wider
    CGFloat    width         = MAX(0.f, self.collectionView.bounds.size.width
                                   - self.interitemSpacing * MAX(0, (NSInteger)columns - 1)) / columns;
    CGFloat    height        = MAX(0.f, rowHeight - self.lineSpacing * MAX(0, (NSInteger)rowsInColumn - 1))
                             / rowsInColumn;

    CGSize size = CGSizeMake(width, height);
    self.cellSizes[visualPath] = [NSValue valueWithCGSize:size];

    return size;
}

#warning recalculation for each cell

// FIXME: cach it? Or use indexPath instead of visualPath in caches?
- (NSIndexPath *)visualPathFromIndexPath:(NSIndexPath *)indexPath
{
    if (![self.delegate respondsToSelector:@selector(numberOfColumsInRow:)]
        ||
        ![self.delegate respondsToSelector:@selector(numberOfRowsInColumn:forRow:)]) {
        NSAssert(NO, @"PatchworkLayoutDelegate should be set.");

        return nil;
    }
    NSUInteger item  = indexPath.row;
    NSUInteger index = 0; // to speed up iteration

    NSMutableArray<NSNumber *> *indexes = [[NSMutableArray alloc] initWithCapacity:kVisualPathLength];

    // TODO: implement OOP wraper for NSIndexPath with arbitrary length
    for (NSUInteger indexesIndex = 0; indexesIndex < kVisualPathLength; indexesIndex++) {
        indexes[indexesIndex] = @0;
    }

    // implementation not fast
    // but general
    // may be optimazed for this specific case
    
    // !!!: in more general case use `for pathElement in 0..<kVisualPathLength` iteration

    // TODO: split to separate methods
    BOOL breakNow = NO; // well... Not such good as recursion, but better then goto :)

    for (NSUInteger row = 0; index <= item && !breakNow; row++) {
        // update row
        indexes[0] = @(row);
        NSUInteger columns = [self.delegate numberOfColumsInRow:row];
        
        // should use recursion in more general case
        // but here just 3 nested loops used...
        
        for (NSUInteger column = 0; column < columns && !breakNow; column++) {
            // update column
            indexes[1] = @(column);
            // get nested rows count
            NSUInteger rowsInColumn = [self.delegate numberOfRowsInColumn:column
                                                                   forRow:row];

            for (NSUInteger rowInColumn = 0; rowInColumn < rowsInColumn && !breakNow; rowInColumn++) {
                // update row in column
                indexes[2] = @(rowInColumn);
                NSLog(@"Row in Column %@", indexes[2]);
                
                if (index == item) {
                    breakNow = YES;
                } else {
                    index++;
                }
            }
        }
    }
    NSUInteger indexesArray[] = {indexes[0].unsignedIntegerValue,
                                 indexes[1].unsignedIntegerValue,
                                 indexes[2].unsignedIntegerValue};
    NSIndexPath *visualPath = [NSIndexPath indexPathWithIndexes:indexesArray
                                                         length:kVisualPathLength];
    NSLog(@"Visual Path %@ for Index Path %@", visualPath, indexPath);
    
    return visualPath;
}

@end
