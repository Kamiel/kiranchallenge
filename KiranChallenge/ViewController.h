//
//  ViewController.h
//  KiranChallenge
//
//  Created by DAXX on 4/7/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController <UICollectionViewDataSource,
                                              UICollectionViewDelegateFlowLayout,
                                              UIBarPositioningDelegate>

@end

