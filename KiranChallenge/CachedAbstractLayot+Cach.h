//
//  NSObject+Cach.h
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "CachedAbstractLayot.h"


@interface CachedAbstractLayot (Cach)

// cache by index path for cell sizes
@property (copy, nonatomic, readonly) NSMutableDictionary<NSIndexPath *, NSValue *> *cellSizes;
// and offsets
@property (copy, nonatomic, readonly) NSMutableDictionary<NSIndexPath *, NSValue *> *cellOffsets;

// cached index path by frame keys
@property (copy, nonatomic, readonly) NSMutableDictionary<NSValue *, NSIndexPath *> *indexPathsByFrame;
// !!!: <<< actually, use CGFrame based on float not really good idea, but it work
// TODO: improve stability here

@end
