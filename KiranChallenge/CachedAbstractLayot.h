//
//  CachedAbstractLayot.h
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "DefaultLayout.h"

/**
 * Current implementation limited by using onli #0 section
 * for real-world usage shuld be implemented any-section-number support.
 */

/// abstract class, should be inherited
@interface CachedAbstractLayot : UICollectionViewLayout

/// must be implemented in subclass
- (CGRect)frameForIndexPath:(NSIndexPath *)indexPath;
/// use MAX for width and for height in subclass
- (CGSize)collectionViewContentSize __attribute__((objc_requires_super));

@property (nonatomic) CGFloat lineSpacing;
@property (nonatomic) CGFloat interitemSpacing;

@end
