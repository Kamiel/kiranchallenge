//
//  CachedAbstractLayot+Cach.m
//  KiranChallenge
//
//  Created by DAXX on 4/21/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "CachedAbstractLayot+Cach.h"
#import "CachedAbstractLayot_Private.h"
#include "Common.h"


@implementation CachedAbstractLayot (Cach)

#pragma mark - properties

- (NSMutableDictionary<NSIndexPath *, NSValue *> *)cellSizes
{
    if (!self->_cellSizes) {
        NSUInteger capacity = ([self.collectionView.dataSource
                                respondsToSelector:@selector(collectionView:numberOfItemsInSection:)])
        ? [self.collectionView.dataSource collectionView:self.collectionView
                                  numberOfItemsInSection:0] // TODO: handle multiply sections (?)
        : 0;
        self->_cellSizes = [[NSMutableDictionary alloc] initWithCapacity:capacity];
    }
    return self->_cellSizes;
}

- (NSMutableDictionary<NSIndexPath *, NSValue *> *)cellOffsets
{
    if (!self->_cellOffsets) {
        NSUInteger capacity = ([self.collectionView.dataSource
                                respondsToSelector:@selector(collectionView:numberOfItemsInSection:)])
        ? [self.collectionView.dataSource collectionView:self.collectionView
                                  numberOfItemsInSection:0]
        : 0;
        self->_cellOffsets = [[NSMutableDictionary alloc] initWithCapacity:capacity];
    }
    return self->_cellOffsets;
}

- (NSMutableDictionary<NSValue *, NSIndexPath *> *)indexPathsByFrame
{
    if (!self->_indexPathsByFrame) {
        NSUInteger capacity = ([self.collectionView.dataSource
                                respondsToSelector:@selector(collectionView:numberOfItemsInSection:)])
        ? [self.collectionView.dataSource collectionView:self.collectionView
                                  numberOfItemsInSection:0]
        : 0;
        self->_indexPathsByFrame = [[NSMutableDictionary alloc] initWithCapacity:capacity];
    }
    return self->_indexPathsByFrame;
}

@end
