//
//  ViewController.m
//  KiranChallenge
//
//  Created by DAXX on 4/7/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import "ViewController.h"
// TODO: use dependency injection here
#import "ImageCell.h"
#import "DefaultLayout.h"
#import "StackLayout.h"
#import "PatchworkLayout.h"

@import ReactiveCocoa;
@import RACAlertAction;

// linking bug here :(
// @see https://github.com/CocoaPods/CocoaPods/issues/5132
//#import <AFNetworking_ImageActivityIndicator/AFNetworking+ImageActivityIndicator.h>

#include "Common.h"


static NSString *const      kCellIdentifier     = @"collectionCell";
static const NSTimeInterval kAnimationDuration  = 0.3;
static const CGFloat        kPatchWorkRowHeight = 300.f; // yep, sorry, it does not respect image size at all


@interface ViewController () <PatchworkLayoutDelegate> {
    NSArray<NSURL *>                           *_imageURLs;
    NSMutableDictionary<NSNumber *, UIImage *> *_images;
    NSMutableDictionary<NSNumber *, NSValue *> *_imageSizesPortait;
    NSMutableDictionary<NSNumber *, NSValue *> *_imageSizesLandscape;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

/* !!!: in real application it should be moved to custom model class, e.g. Mantle
 ********************************************************************************/
@property (strong, nonatomic, readonly) NSArray<NSURL *>                           *imageURLs;
@property (copy, atomic, readonly)      NSMutableDictionary<NSNumber *, UIImage *> *images; // using it as sparce array

- (UIImage *)imageForItemAtIndexPath:(NSIndexPath *)indexPath; // return value from sparce array or default if nil
/********************************************************************************/

// outlets
@property (weak, nonatomic) IBOutlet UIBarButtonItem  *layoutButtonItem;
@property (weak, nonatomic) IBOutlet UINavigationItem *layoutLabelItem;

- (IBAction)changeLayout:(id)sender;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // TODO: start download images right here (?)

    // some ReactiveCocoa magic
    self.layoutButtonItem.rac_command = [[RACCommand alloc] initWithEnabled:[RACSignal return:@YES]
                                                                signalBlock:^RACSignal *(UIBarButtonItem *buttonItem) {
                                                                    [self changeLayout:buttonItem];
                                                                    
                                                                    return [RACSignal empty];
                                                                }];
    // pull to refresh
    UIRefreshControl *refreshControl = UIRefreshControl.new;
    refreshControl.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [self.images removeAllObjects];
        [self.collectionView reloadData];
        
        return [RACSignal empty];
    }];
    [self.collectionView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self->_imageURLs = nil;
    self->_images    = nil;
}

#pragma mark - properties

- (NSMutableDictionary<NSNumber *,UIImage *> *)images
{
    NSMutableDictionary<NSNumber *,UIImage *> *images;
    @synchronized(self) {
        if (!self->_images) {
            self->_images = [[NSMutableDictionary alloc] initWithCapacity:self.imageURLs.count];
        }
        images = self->_images;
    }
    return images;
}

- (NSArray<NSURL *> *)imageURLs
{
    if (!self->_imageURLs) {
        const NSUInteger imagesCount = 25; // yep, magic number from task description
        NSMutableArray<NSURL *> *imageURLs = [[NSMutableArray alloc] initWithCapacity:imagesCount];
        [@[@"https://i.imgur.com/eyrags4.jpg", // TODO: put in plist at resources
           @"https://i.imgur.com/KsFWYcu.jpg",
           @"https://i.imgur.com/X2OUjWu.png",
           @"https://i.imgur.com/o3WVhY8.jpg",
           @"https://i.imgur.com/0qkbWJ6.jpg",
           
           @"https://i.imgur.com/htLE2U6.jpg",
           @"https://i.imgur.com/6tnuWwJ.jpg",
           @"https://i.imgur.com/BwojHWr.jpg",
           @"https://i.imgur.com/q56mq67.jpg",
           @"https://i.imgur.com/LDCRTGw.jpg",
           
           @"https://i.imgur.com/pp7RE6q.jpg",
           @"https://i.imgur.com/ZTFzxlG.jpg",
           @"https://i.imgur.com/Ehf0CWM.jpg",
           @"https://i.imgur.com/Vhz5Tya.jpg",
           @"https://i.imgur.com/PJAGcQa.jpg",
           
           @"https://i.imgur.com/TTxnWm6.jpg",
           @"https://i.imgur.com/89QJPzA.jpg",
           @"https://i.imgur.com/c3JM11x.jpg",
           @"https://i.imgur.com/dZAArPs.jpg",
           @"https://i.imgur.com/XKEOKru.jpg",
           
           @"https://i.imgur.com/OHFT7YL.jpg",
           @"https://i.imgur.com/aAfYV9I.jpg",
           @"https://i.imgur.com/ANyFvKB.jpg",
           @"https://i.imgur.com/mkLHbex.jpg",
           @"https://i.imgur.com/EE8Zn8T.jpg"]
         enumerateObjectsUsingBlock:^(NSString *path, NSUInteger idx, BOOL * _Nonnull stop) {
             if (!path) {
                 return;
             }
             [imageURLs addObject:[NSURL URLWithString:path]];
         }];
        self->_imageURLs = [imageURLs copy];
    }
    return self->_imageURLs;
}

#pragma mark - private methods

- (UIImage *)imageForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.images[@(indexPath.row)] ?: [ImageCell defaultImage];
}

#pragma mark - UIBarPositioningDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

#pragma mark - override UIViewController (UIViewControllerRotation) methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.collectionView.collectionViewLayout invalidateLayout];
    }
                                 completion:nil];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return self.imageURLs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ({
        ImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier
                                                                    forIndexPath:indexPath];
        UIImage *image = self.images[@(indexPath.row)];
        
        if (image) { // cached image
            cell.imageView.image = image;
            return cell;
        }
        [cell cellWithImageFromURL:self.imageURLs[indexPath.row]
                        completion:^{
                            self.images[@(indexPath.row)] = cell.imageView.image;
                            // update cell's size
                            [UIView animateWithDuration:kAnimationDuration
                                             animations:^{
                                                 [self.collectionView.collectionViewLayout invalidateLayout];
                                             }];
                        }];
    });
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize imageSize   = [self imageForItemAtIndexPath:indexPath].size;
    CGFloat width      = self.collectionView.bounds.size.width;
    
    return (imageSize.width > width) ? ({
        CGFloat proportion = imageSize.height / imageSize.width;
        // aspect fit to screen width, little hack to handle big images
        CGSize aspectFit  = CGSizeMake(width, width * proportion);
        
        aspectFit;
    })
    : imageSize;
}

#pragma mark - PatchworkLayoutDelegate

- (NSUInteger)numberOfColumsInRow:(NSUInteger)row
{
    // patchwork structure according to screenshot
    // https://s3.amazonaws.com/f.cl.ly/items/0Y3s2E3Y3f0M1p1F2e2n/Image%202016-04-19%20at%2010.48.05%20am.jpg?v=ddb07ad8
    //
    // and use more rows for ladscape
    
    return UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])
    ? 2  // for portait
    : 4; // and landscape
}

- (NSUInteger)numberOfRowsInColumn:(NSUInteger)column forRow:(NSUInteger)row
{
    BOOL (^odd)(NSUInteger number) = ^(NSUInteger number){ return (BOOL)(number % 2); };

    return odd(row) != odd(column) ? 2 : 1;
}

- (CGFloat)rowHeight
{
    return kPatchWorkRowHeight;
}

#pragma mark - IBActions

- (IBAction)changeLayout:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Change Layout"
                                                                   message:@"Select what you like!"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];

    RACAlertAction *nevermind     = [RACAlertAction actionWithTitle:@"Nevermind" style:UIAlertActionStyleCancel];
    RACAlertAction *defaultLayout = [RACAlertAction actionWithTitle:@"Default" style:UIAlertActionStyleDefault];
    RACAlertAction *stackLayout   = [RACAlertAction actionWithTitle:@"Stack" style:UIAlertActionStyleDefault];
    RACAlertAction *patchworkkLayout   = [RACAlertAction actionWithTitle:@"Patchwork" style:UIAlertActionStyleDefault];
    
    [alert addAction:nevermind];
    [alert addAction:defaultLayout];
    [alert addAction:stackLayout];
    [alert addAction:patchworkkLayout];
    
    defaultLayout.command = [[RACCommand alloc] initWithEnabled:[RACSignal return:@YES]
                                                    signalBlock:^RACSignal *(RACAlertAction *action) {
                                                        [self.collectionView setCollectionViewLayout:DefaultLayout.new
                                                                                            animated:YES];
                                                        self.layoutLabelItem.title = action.title;

                                                        return [RACSignal empty];
                                                    }];
    stackLayout.command = [[RACCommand alloc] initWithEnabled:[RACSignal return:@YES]
                                                  signalBlock:^RACSignal *(RACAlertAction *action) {
                                                      [self.collectionView setCollectionViewLayout:StackLayout.new
                                                                                          animated:YES];
                                                      self.layoutLabelItem.title = action.title;
                                                      
                                                      return [RACSignal empty];
                                                  }];
    patchworkkLayout.command = [[RACCommand alloc] initWithEnabled:[RACSignal return:@YES]
                                                       signalBlock:^RACSignal *(RACAlertAction *action) {
                                                           [self.collectionView setCollectionViewLayout:({
                                                               PatchworkLayout *patchwork = PatchworkLayout.new;
                                                               patchwork.delegate = self;
                                                               patchwork;
                                                           })
                                                                                               animated:YES];
                                                           self.layoutLabelItem.title = action.title;
                                                           
                                                           return [RACSignal empty];
                                                       }];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
