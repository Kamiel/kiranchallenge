//
//  ImageCell.h
//  KiranChallenge
//
//  Created by DAXX on 4/7/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

+ (UIImage *)defaultImage;
+ (UIImage *)failureImage;

- (instancetype)cellWithImageFromURL:(NSURL *)URL
                          completion:(void (^)())completion;

@end
