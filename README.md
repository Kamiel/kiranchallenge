# Kiran's Challenge

Hi, Kiran!  **ヾ(▱‿▱　)ノ**

Here you can see some sources.

- I used `ReactiveCocoa` in `ViewController` to handle user actions.
- Here user can choose form 3 layouts:
    - default, it presented by default `UICollectionViewFlowLayout`.  No additional code here was not needed.
    - stack, with custom offsets  for cells,one above another.
    - patchwork, see description below.

`StackLayout` and `PatchworkLayout` both inherited from `CachedAbstractLayot`, which just abstract class that encapsulate cache logic. `CachedAbstractLayot` implemented by inheriting standard `UICollectionViewLayout`.


In `CachedAbstractLayout` used a lot of in-memory caches to speedup collection view performance and improve UX.  But some little further optimisation here yet possible.


In case of plenty images, real world implementation should store some of them in local caches on hard drive (e.g. in CoreData) and clean up memory when needed.  Also this example not use NSCache to cache image requests in proper way (depend on server side, how long it should be stored).

***

## *UPD:* According to your feedback

* Problem I am seeing now is that the two layout modes are identical.
    * Well, these layouts are not exactly the same (as you can see in code).  Just check the landscape mode to be sure. Bun both of them respect image sizes, and don't change it, yep (just aspect fit to collection view width).
* Mode 2: Multiple images per row, with some images as a large size and some as a smaller size, fitting together like a patchwork.  You can find an example [here](https://s3.amazonaws.com/f.cl.ly/items/0Y3s2E3Y3f0M1p1F2e2n/Image%202016-04-19%20at%2010.48.05%20am.jpg?v=ddb07ad8).
    * Still have some questions
        * Correct me, please, if I mistaken: this layout should not respect image size at all (even aspect ratio).
        * Layout should be customizable by delegate (but how generalized it should be?).
            * Most flexable solution will be use Swift's generalized algebraic datatype to describe tree-like recursive value of `columnsForRow:inRow:inColumn:inRow...etc` collection cell arrangement style.
            * But for now I'll use protocol which customized by 2 simple methods, `numberOfColumsInRow:` and `numberOfRowsInColumn:forRow:`.

***

BTW, I have one yet idea how to improve performance: load images not just one by one, but as a queue with priorities (pause offscreen traffic).

* Investigated a little this case.
    * [Here](https://github.com/AFNetworking/AFNetworking/pull/704) @mattt wrote that

    > I found that simply cancelling image requests when they went offscreen was much more effective than attempting to keep partial image data around

   * On other hand, [here](http://cocoanuts.mobi/2014/04/27/fastscroll/) you may found low-level solution, which may be useful in case of big image loading.

   * Should be investigated deeper, maybe `NSCache` will be more handy.

***

Also, I have add:

- nice image fade effect
- pull to refresh to empty image cache.

***

What I haven't time to do:

- use `shouldRasterize` on cell
- round float numbers in frames to avoid offscreen rendering
- resize images just after downloading
- `PatchworkLayout` yet require refactoring and little optimization.

***

And, as always, thank you for nice chance to get some interesting experience.


⌨**ヾ(□_͜ □ )**
